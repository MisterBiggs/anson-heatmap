# anson-heatmap

Using a modified version of [Geo Heatmap](https://github.com/luka1199/geo-heatmap) and location data from Google Takeout I was able to generate this map of everywhere I've been down  to the millisecond. Thank you Google for datahoarding to an almost *disgusting* extent. 


## Heatmap

```python
gradient={
    0.0: "rgb(72, 0, 84)",
    0.14285714285714285: "rgb(79, 48, 127)",
    0.2857142857142857: "rgb(67, 91, 141)",
    0.42857142857142855: "rgb(52, 127, 142)",
    0.5714285714285714: "rgb(34, 162, 135)",
    0.7142857142857142: "rgb(61, 195, 108)",
    0.8571428571428571: "rgb(147, 219, 53)",
    1.0: "rgb(243, 233, 28)",
}
```

## Command 

`python .\geo_heatmap.py .\takeout-STUFFHERE-001.zip -m StamenToner`